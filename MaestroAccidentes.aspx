﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="MaestroAccidentes.aspx.cs" Inherits="MaestroAccidentes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <%--INICIO--%>
    <h2>Maestro Accidentes</h2>
    <asp:UpdatePanel ID="PanelPrincipal" runat="server">
        <ContentTemplate>
            <asp:UpdatePanel ID="PaneldeBusqueda" runat="server">
                <ContentTemplate>

                    <table cellpadding="0" cellspacing="0" border="0" width="90%" align="center">
                        <tr>
                            <td colspan="3">
                                <asp:Button ID="Button4" runat="server" CssClass="btn btn-warning btn-sm  " OnClick="Button4_Click"
                                    Text="Nuevo Registro" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Buscar por nombre del trabajador
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtBusqueda" runat="server" CssClass=" form-control w-100 "></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Buscar" CssClass="btn btn-warning btn-sm  float-end" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center">
                                &nbsp;
                            </td>
                            <td align="right">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="divGrid" style="overflow: auto; height: auto; width: auto;">
                                    <asp:DataGrid ID="DataGrid1" runat="server" CssClass="table table-striped table-bordered" OnItemCommand="Grid_Ejemplo">
                                        <Columns>
                                            <asp:ButtonColumn CommandName="Select" Text="Seleccionar"></asp:ButtonColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="Error1" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <asp:UpdatePanel ID="PaneldeEdicion" runat="server">
                <ContentTemplate>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="900px">
                        <tr>
                            <td>
                                Fecha Accidente
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TxtFecha1" ReadOnly="true" runat="server" CssClass=" form-control"></asp:TextBox>
                                <br />
                                <asp:Calendar ID="Calendar1" runat="server" TargetControlID="TxtFecha1" onselectionchanged="Calendar1_SelectionChanged" ></asp:Calendar>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Lugar ocurrencia
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Labor realizada
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Supervisor Faena
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                Caracteristicas utileria
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<asp:DropDownList ID="DropDownList2" runat="server">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="TextBox3" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Equipos de levante
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox4" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nombre del trabajador
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>

                                <%--<asp:TextBox ID="TextBox6" runat="server" CssClass=" form-control"></asp:TextBox>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Función del trabajador
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox5" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Descripción accidente
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox6" runat="server" CssClass=" form-control" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Lesiones del trabajador
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox7" runat="server" CssClass=" form-control" MaxLength="1000" TextMode="MultiLine" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fuente del accidente
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox8" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Agente
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox9" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tipo
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox10" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Acción subestandar
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox11" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Condición subestandar
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox12" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Factor personal
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox13" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Factor trabajo
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox14" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Falta de control
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList3" runat="server" CssClass="dropdown" Width="100%">

                                </asp:DropDownList>

                                <%--<asp:TextBox ID="TextBox15" runat="server" CssClass=" form-control"></asp:TextBox>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Acción Correctiva
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox15" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>


                        <tr>
                            <td>
                                Cargo User
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<asp:TextBox ID="TextBox16" runat="server" CssClass=" form-control"></asp:TextBox>--%>
                                <asp:DropDownList ID="DropDownList4" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <br />
                                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Grabar" CssClass="btn btn-warning btn-sm  " />
                                &nbsp;<asp:Button ID="Button3" runat="server" OnClick="Button3_Click" CssClass="btn btn-warning btn-sm  "
                                    Text="Volver" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Error2" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--FIN--%>
</asp:Content>
