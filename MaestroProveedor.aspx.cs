﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;

public partial class MaestroProveedor : System.Web.UI.Page
{
    protected ClassConexion conexion = new ClassConexion();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Load de la Pagina
        if (!IsPostBack)
        {
            Context.Session["idRegistro"] = "";
            Context.Session["graba"] = "";
            PaneldeBusqueda.Visible = true;
            PaneldeEdicion.Visible = false;
            //
            try
            {
                //Paneles Seguridad
                //Buscar
                Button1.Visible = false;
                //Grabar
                Button2.Visible = false;
                //Volver
                Button3.Visible = false;
                //Nuevo Registro
                Button4.Visible = false;

                string sql = null;
                //string sql = "";
                sql = "Select top 1 * from tbPaginaPermiso where UsuarioCorre = '" + Context.Session["User"].ToString() + "' and DescripcionPaginaURL = 'MaestroProveedor.aspx' and EstadoActivo = 'Activado'";
                //
                DataTable dt = new DataTable();
                dt = conexion.ListarQueryDt(sql);

                if (dt.Rows.Count == 0)
                {
                    Response.Redirect("CloseUP.aspx");
                }

                string TipoUsuario = "";

                foreach (DataRow ds in dt.Rows)
                {
                    TipoUsuario = ds[5].ToString().ToUpper();
                }

                switch (TipoUsuario)
                //switch (Context.Session["Tipo$_$Usuario"].ToString().ToUpper())
                {
                    case "SUPERUSUARIO":
                        Button1.Visible = true;
                        Button2.Visible = true;
                        Button3.Visible = true;
                        Button4.Visible = true;
                        break;
                    default:
                        Response.Redirect("CloseUP.aspx");
                        break;
                }

            }
            catch (Exception ex)
            {
                Response.Redirect("ClosedUP.aspx");
            }
        }
        //variable de session
        //Context.Session["idRegistro"] -> esta almacena el Id del registro de la base de datos
        //Context.Session["grabar"] = 1 -> estado de un nuevo registro
        //Context.Session["grabar"] = 2 -> estado para modificar un registro
    }

    //

    //
    protected void Button1_Click(object sender, EventArgs e)
    {
        string sql = null;
        DataTable dt = new DataTable();
        try
        {
            //sql = " Select * from tbProveedores";
            sql = "select pv.IdPrv as ID, pv.RazonPrv as 'Razon Social', pv.RutPrv as Rut, pv.NombrePrv as 'Nombre', pv.telPrv as 'Telefono', pv.correoPrv as 'Correo', oc.OrdenCompra as 'Orden de compra', tc.DescripcionContrato as 'Tipo contrato' from tbProveedores pv inner join tbOrdenCompra oc ON pv.OrdenCompraPrv = oc.IdOc inner join tbTipoContrato tc ON pv.ContratoAsoPrv = tc.idContrato";
            if (txtBusqueda.Text != "")
            {
                sql += " where razonprv like '%" + txtBusqueda.Text + "%'";
            }
            try
            {
                dt = conexion.ListarQueryDt(sql);
            }
            catch (Exception ex)
            {
                Error1.Text += ex.Message;
            }

            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            Error1.Text = Ex.Message;
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        ValidadordeCaracteresEspeciales();
        string sql = null;
        string EvaluaciondeFUncion = Context.Session["graba"].ToString();
        if (EvaluaciondeFUncion.ToString() == "1")
        {
            sql = " insert into [tbProveedores] (IdPrv, RazonPrv, RutPrv, NombrePrv, TelPrv, CorreoPrv, OrdenCompraPrv, ContratoAsoPrv) values (( select isnull(max(IdPrv),0) + 1 from  [tbProveedores] ) , '" + TextBox1.Text.Trim() + "', '" + TextBox2.Text.Trim() + "', '" + TextBox3.Text.Trim() + "', '" + TextBox4.Text.Trim() + "', '" + TextBox5.Text.Trim() + "', '" + DropDownList1.SelectedValue + "','" + DropDownList2.SelectedValue + "' )";

        }
        if (EvaluaciondeFUncion.ToString() == "2")
        {
            sql = " update [tbProveedores] set RazonPrv = '" + TextBox1.Text.Trim() + "'  , RutPrv = '" + TextBox2.Text.Trim() + "', NombrePrv = '" + TextBox3.Text.Trim() + "', TelPrv = '" + TextBox4.Text.Trim() + "', correoPrv = '" + TextBox5.Text.Trim() + "', OrdenCompraPrv= '" + DropDownList1.SelectedValue + "', ContratoAsoPrv= '" + DropDownList2.SelectedValue + "' where  IdPrv =   " + Context.Session["idRegistro"].ToString();
        }

        try
        {
            conexion.ExecuteSql(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }

    //
    protected void Button3_Click(object sender, EventArgs e)
    {
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Context.Session["graba"] = "1";
        //Limpiar y Cargar Combox
        //DropDownList1.Items.Clear();
        ComboBox();
        ComboBoxContrato();
        //ComboBoxEstadoActivo();
        //Limpiar Formulario
        limpiar();
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
    }

    //
    //Grilla
    protected void Grid_Ejemplo(Object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            PaneldeBusqueda.Visible = false;
            PaneldeEdicion.Visible = true;
            Context.Session["idRegistro"] = e.Item.Cells[1].Text;
            //Limpiar y Cargar Combox
            //Limpiar Formulario
            DropDownList1.Items.Clear();
            DropDownList2.Items.Clear();
            ComboBox();
            ComboBoxContrato();
            //limpiar();
            CargarFormulario(e.Item.Cells[1].Text);
        }
    }

    //Metodos

    protected DataTable CargarFormulario(string a)
    //protected DataTable CargarFormulario()
    {
        //Carga Formulario 
        DataTable dt = new DataTable();
        string sql = "";
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
        try
        {
            sql = "select * from tbproveedores where [IdPrv] = " + a.ToString();
            dt = conexion.ListarQueryDt(sql);
            // ComboBox();
            // limpiar();
            foreach (DataRow dr in dt.Rows)
            {
                TextBox1.Text = dr[1].ToString().Trim();
                TextBox2.Text = dr[2].ToString().Trim();
                TextBox3.Text = dr[3].ToString().Trim();
                TextBox4.Text = dr[4].ToString().Trim();
                TextBox5.Text = dr[5].ToString().Trim();
                DropDownList1.SelectedValue = dr[6].ToString();
                DropDownList2.SelectedValue = dr[7].ToString();
            }
            Context.Session["graba"] = "2";
        }
        catch (Exception Ex)
        {

            this.Error1.Text = Ex.Message;

        }
        return dt;
    }

    //Combo Empresa
    protected int ComboBox()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select idoc, ordencompra from tbordencompra";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList1.Items.Clear();
        DropDownList1.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList1.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    // Contrato Servicio Vigencia
    protected int ComboBoxContrato()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select *  from tbtipocontrato";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList2.Items.Clear();
        DropDownList2.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList2.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //
    protected int limpiar()
    {
        ComboBox();
        ComboBoxContrato();
        int a = 0;
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        TextBox5.Text = "";
        DropDownList1.SelectedValue = "0";
        DropDownList2.SelectedValue = "0";
        return a;
    }

    protected int ValidadordeCaracteresEspeciales()
    {
        int a = 0;
        //Quita Valores Especiales del texbox
        //for (int i = 1; i <= 2; i++)
        //{
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("'", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¡", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¿", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("!", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("?", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(",", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(".", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("´", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("`", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(";", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("+", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("-", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("<", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(">", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("=", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("&", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("%", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("$", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("#", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("/", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Trim();
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.ToUpper();
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("SELECT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DELETE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("UPDATE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("INSERT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("ALL ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("TABLE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DROP ", "");

        //    if (((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text == "")
        //    {
        //        ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = "Sin Datos";
        //    }
        //}
        ////llena con un cero
        //if (TextBox3.Text == "")
        //{
        //    TextBox3.Text = "0";
        //}
        return a;
    }

    protected int limpiarFormularioBusqueda()
    {
        //Limpia el Formulario de Busqueda
        txtBusqueda.Text = null;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        return 1;
    }

    //
}