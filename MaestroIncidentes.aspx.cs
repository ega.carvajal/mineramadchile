﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data;

public partial class MaestroIncidentes : System.Web.UI.Page
{
    //private string lineaConexion = null;
    protected ClassConexion conexion = new ClassConexion();
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        //Load de la Pagina
        if (!IsPostBack)
        {
            //System.Web.HttpContext.Current.Response.Write(Context.Session["User"].ToString());
            //
            Context.Session["idRegistro"] = "";
            Context.Session["graba"] = "";
            //
            //Inicio Copia
            try
            {
                //Paneles Seguridad
                PaneldeBusqueda.Visible = true;
                PaneldeEdicion.Visible = false;
                //Buscar
                Button1.Visible = false;
                //Grabar
                Button2.Visible = false;
                //Volver
                Button3.Visible = false;
                //Nuevo Registro
                Button4.Visible = false;

                string sql = null;
                //string sql = "";
                sql = "Select top 1 * from tbPaginaPermiso where UsuarioCorre = '" + Context.Session["User"].ToString() + "' and DescripcionPaginaURL = 'MaestroIncidentes.aspx' and EstadoActivo = 'Activado'";
                //
                DataTable dt = new DataTable();
                dt = conexion.ListarQueryDt(sql);

                if (dt.Rows.Count == 0)
                {
                    Response.Redirect("CloseUP.aspx");
                }

                string TipoUsuario = "";

                foreach (DataRow ds in dt.Rows)
                {
                    TipoUsuario = ds[5].ToString().ToUpper();
                }

                switch (TipoUsuario)
                {
                    case "SUPERUSUARIO":
                        Button1.Visible = true;
                        Button2.Visible = true;
                        Button3.Visible = true;
                        Button4.Visible = true;
                        break;
                    default:
                        Response.Redirect("CloseUP.aspx");
                        break;
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("ClosedUP.aspx");
                //Error1.Text = ex.Message;

            }
            //Coppia fin

            //PaneldeBusqueda.Visible = true;
            //PaneldeEdicion.Visible = false;
            //ComboBox();
        }
    }

    //Botones

    protected void Button1_Click(object sender, EventArgs e)
    {
        Error1.Text = "";
        //Buscar y carga datagrid
        string sql = null;
        DataTable dt = new DataTable();
        //conexion a la base de datos y comando de carga del datagrib
        try
        {
            //sql = "select * from tbIncidentes ";
            sql = " select ";
            sql += " id as [ID SISTEMA] ";
            sql += ", nombreEncargado as 'Nombre del encargado' ";
            sql += ", fecha as 'Fecha incidente' ";
            sql += ", lugar as 'Lugar incidente' ";
            sql += ", descripcion as 'Descripcion del incidente' ";
            sql += ", accInmediata as 'Acción Inmediata' ";
            sql += ", causaAcc as 'Causa Accidente' ";
            sql += ", accPreventiva as 'Acción Preventiva' ";
            sql += ", evaluacionRiesgo as 'Evaluación del Riesgo' ";
            sql += ", implementacion as 'Implementación' ";
            sql += ", verificacion as 'Verificación' ";
            sql += " from tbIncidentes ";

            if (txtBusqueda.Text != "")
            {
                sql += " where nombreEncargado like '%" + txtBusqueda.Text + "%'";
            }
            //Cargando Datagrid
            try
            {
                dt = conexion.ListarQueryDt(sql);
            }
            catch (Exception ex)
            {
                Error1.Text += ex.Message;
            }

            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            //Mensaje de Error desde la Base de datos.
            Error1.Text = Ex.Message;
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        ValidadordeCaracteresEspeciales();
        string sql = null;
        string EvaluaciondeFUncion = Context.Session["graba"].ToString();
        if (EvaluaciondeFUncion.ToString() == "1")
        {
            sql = "insert into [tbIncidentes] (id, nombreEncargado, fecha, lugar, descripcion, accInmediata, causaAcc, accPreventiva, evaluacionRiesgo, implementacion, verificacion) values ((select isnull(max(id),0) +1 from [tbIncidentes]), '" + DropDownList1.SelectedItem.Text + "', '" + TxtFecha1.Text.Trim() + "', '" + TextBox2.Text.Trim() + "', '" + TextBox3.Text.Trim() + "','" + TextBox4.Text.Trim() + "','" + TextBox5.Text.Trim() + "','" + TextBox6.Text.Trim() + "','" + TextBox7.Text.Trim() + "','" + TextBox8.Text.Trim() + "','" + TextBox9.Text.Trim() + "') "; //,'" + txtFecha2.Text.Trim() + "'    
        }
        if (EvaluaciondeFUncion.ToString() == "2")
        {
            sql = "update [tbIncidentes] set nombreEncargado = '" + DropDownList1.SelectedItem.Text + "', fecha = '" + TxtFecha1.Text.Trim() + "', lugar = '" + TextBox2.Text.Trim() + "', descripcion = '" + TextBox3.Text.Trim() + "', accInmediata = '" + TextBox4.Text.Trim() + "', causaAcc = '" + TextBox5.Text.Trim() + "', accPreventiva = '" + TextBox6.Text.Trim() + "', evaluacionRiesgo = '" + TextBox7.Text.Trim() + "', implementacion = '" + TextBox8.Text.Trim() + "', verificacion = '" + TextBox9.Text.Trim() + "' where  id =   " + Context.Session["idRegistro"].ToString(); //, fechaCierre = '" + txtFecha2.Text.Trim() + "'
        }
        try
        {
            conexion.ExecuteSql(sql);
            //EnviarMensaje4();
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        //
        Context.Session["graba"] = "1";
        //Limpiar Formulario
        limpiar();
        ComboBox();
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
    }

    //Grilla
    protected void Grid_Ejemplo(Object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            PaneldeBusqueda.Visible = false;
            PaneldeEdicion.Visible = true;
            Context.Session["idRegistro"] = e.Item.Cells[1].Text;
            //Limpiar Formulario
            ComboBox();
            limpiar();
            CargarFormulario(e.Item.Cells[1].Text);
        }
    }

    //Metodos

    protected DataTable CargarFormulario(string a)
    //protected DataTable CargarFormulario()
    {
        //Carga Formulario 
        DataTable dt = new DataTable();
        string sql = "";
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
        try
        {
            sql = "select * from tbIncidentes where id = " + a.ToString();
            dt = conexion.ListarQueryDt(sql);
            // ComboBox();
            // limpiar();
            foreach (DataRow dr in dt.Rows)
            {

                DropDownList1.SelectedItem.Text = dr["nombreEncargado"].ToString();
                //TextBox1.Text = dr[1].ToString().Trim();
                TxtFecha1.Text = dr["fecha"].ToString().Trim();
                TextBox2.Text = dr["lugar"].ToString().Trim();
                TextBox3.Text = dr["descripcion"].ToString().Trim();
                TextBox4.Text = dr["accInmediata"].ToString().Trim();
                TextBox5.Text = dr["causaAcc"].ToString().Trim();
                TextBox6.Text = dr["accPreventiva"].ToString().Trim();
                TextBox7.Text = dr["evaluacionRiesgo"].ToString().Trim();
                TextBox8.Text = dr["implementacion"].ToString().Trim();
                TextBox9.Text = dr["verificacion"].ToString().Trim();
                //txtFecha2.Text = dr["fechaCierre"].ToString().Trim();
            }
            Context.Session["graba"] = "2";
        }
        catch (Exception Ex)
        {

            this.Error1.Text = Ex.Message;

        }
        return dt;
    }

    //Combo Encargado
    protected int ComboBox()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select id, nombreTrabajador+' '+apellidoTrabajador as [Nombre]  from tbTrabajador";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList1.Items.Clear();
        DropDownList1.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList1.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //
    protected int limpiar()
    {
        ComboBox();
        int a = 0;
        TxtFecha1.Text = "";
        //TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        TextBox5.Text = "";
        TextBox6.Text = "";
        TextBox7.Text = "";
        TextBox8.Text = "";
        TextBox9.Text = "";
        return a;
    }

    protected int ValidadordeCaracteresEspeciales()
    {
        int a = 0;

        return a;
    }

    protected int limpiarFormularioBusqueda()
    {
        //Limpia el Formulario de Busqueda
        txtBusqueda.Text = null;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        return 1;
    }

    //Calendarios
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        TxtFecha1.Text = Calendar1.SelectedDate.ToString("dd-MM-yyyy");
        Calendar1.Visible = true;
        
        //Calendar1.Visible = true;
    }

}