﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data;

public partial class MaestroAccidentes : System.Web.UI.Page
{
    protected ClassConexion conexion = new ClassConexion();
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        //Load de la Pagina
        if (!IsPostBack)
        {
            //System.Web.HttpContext.Current.Response.Write(Context.Session["User"].ToString());
            //
            Context.Session["idRegistro"] = "";
            Context.Session["graba"] = "";
            //
            //Inicio Copia
            try
            {
                //Paneles Seguridad
                PaneldeBusqueda.Visible = true;
                PaneldeEdicion.Visible = false;
                //Buscar
                Button1.Visible = false;
                //Grabar
                Button2.Visible = false;
                //Volver
                Button3.Visible = false;
                //Nuevo Registro
                Button4.Visible = false;

                string sql = null;
                //string sql = "";
                sql = "Select top 1 * from tbPaginaPermiso where UsuarioCorre = '" + Context.Session["User"].ToString() + "' and DescripcionPaginaURL = 'MaestroAccidentes.aspx' and EstadoActivo = 'Activado'";
                //
                DataTable dt = new DataTable();
                dt = conexion.ListarQueryDt(sql);

                if (dt.Rows.Count == 0)
                {
                    Response.Redirect("CloseUP.aspx");
                }

                string TipoUsuario = "";

                foreach (DataRow ds in dt.Rows)
                {
                    TipoUsuario = ds[5].ToString().ToUpper();
                }

                switch (TipoUsuario)
                {
                    case "SUPERUSUARIO":
                        Button1.Visible = true;
                        Button2.Visible = true;
                        Button3.Visible = true;
                        Button4.Visible = true;
                        break;
                    default:
                        Response.Redirect("CloseUP.aspx");
                        break;
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("ClosedUP.aspx");
                //Error1.Text = ex.Message;

            }
            //Coppia fin

            //PaneldeBusqueda.Visible = true;
            //PaneldeEdicion.Visible = false;
            //ComboBox();
        }
    }

    //Botones

    protected void Button1_Click(object sender, EventArgs e)
    {
        Error1.Text = "";
        //Buscar y carga datagrid
        string sql = null;
        DataTable dt = new DataTable();
        //conexion a la base de datos y comando de carga del datagrib
        try
        {
            //sql = "select * from tbCliente ";
            sql = "select ";
            sql += " id as [ID SISTEMA] ";
            sql += ", fechaAcc as 'Fecha Accidente' ";
            sql += ", lugarOcurrencia as 'Lugar ocurrencia' ";
            sql += ", laborRealizada as 'Labor Realizada' ";
            sql += ", supervisorFaena as 'Supervisor de la Faena' ";
            sql += ", caractUtilerias as 'Caracteristicas de Utilerias' ";
            sql += ", equiposLevante as 'Equipos de levante' ";
            sql += ", nombreTrabajador as 'Nombre del trabajador' ";
            sql += ", funcionTrabajador as 'Función del trabajador' ";
            sql += ", descripcionAcc as 'Descripción del Accidente' ";
            sql += ", lesiones as Lesiones ";
            sql += ", fuenteAcc as 'Fuente del Accidente' ";
            sql += ", Agente as Agente ";
            sql += ", tipo as Tipo ";
            sql += ", accionSubestandar as 'Acción Subestandar' ";
            sql += ", condicionSubestandar as 'Condición Subestandar' ";
            sql += ", factorPersonal as 'Factor Personal' ";
            sql += ", factorTrabajo as 'Factor del Trabajo' ";
            sql += ", faltaControl as 'Falta de control' ";
            sql += ", accionCorrectiva as 'Acción Correctiva' ";
            sql += ", nombreUser as 'Nombre de usuario' ";
            sql += ", cargoUser as 'Cargo del usuario' ";
            
            sql += " from tbAccidente ";
            if (txtBusqueda.Text != "")
            {
                sql += " where nombreTrabajador like '%" + txtBusqueda.Text + "%'";
            }
            //Cargando Datagrid
            try
            {
                dt = conexion.ListarQueryDt(sql);
            }
            catch (Exception ex)
            {
                Error1.Text += ex.Message;
            }

            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            //Mensaje de Error desde la Base de datos.
            Error1.Text = Ex.Message;
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        ValidadordeCaracteresEspeciales();
        string sql = null;
        string EvaluaciondeFUncion = Context.Session["graba"].ToString();
        if (EvaluaciondeFUncion.ToString() == "1")
        {
            sql = "insert into [tbAccidente] (id, fechaAcc, lugarOcurrencia, laborRealizada, supervisorFaena, caractUtilerias, equiposLevante, nombreTrabajador, funcionTrabajador";
            sql += ", descripcionAcc, lesiones, fuenteAcc, Agente, tipo, accionSubestandar, condicionSubestandar, factorPersonal, factorTrabajo, faltaControl, accionCorrectiva";
            sql += ", nombreUser, cargoUser) values ((select isnull(max(id),0) +1 from [tbAccidente])";//, fechaCorrectiva, fechaVerificacion, fechaCierre
            sql += ", '" + TxtFecha1.Text.Trim() + "','" + TextBox1.Text.Trim() + "' , '" + TextBox2.Text.Trim() + "', '" + DropDownList1.SelectedItem.Text + "', '" + TextBox3.Text.Trim() + "','" + TextBox4.Text.Trim() + "' ";
            sql += ", '" + DropDownList2.SelectedItem.Text + "','" + TextBox5.Text.Trim() + "','" + TextBox6.Text.Trim() + "','" + TextBox7.Text.Trim() + "','" + TextBox8.Text.Trim() + "','" + TextBox9.Text.Trim() + "' ";
            sql += ", '" + TextBox10.Text.Trim() + "', '" + TextBox11.Text.Trim() + "', '" + TextBox12.Text.Trim() + "', '" + TextBox13.Text.Trim() + "', '" + TextBox14.Text.Trim() + "', '" + DropDownList3.SelectedItem.Text + "' ";
            sql += ", '" + TextBox15.Text.Trim() + "', '" + Context.Session["User"].ToString() + "', '" + DropDownList4.SelectedItem.Text + "' )";
        }
        if (EvaluaciondeFUncion.ToString() == "2")
        {
            sql = "update [tbAccidente] set fechaAcc = '" + TxtFecha1.Text.Trim() + "', lugarOcurrencia = '" + TextBox1.Text.Trim() + "', laborRealizada = '" + TextBox2.Text.Trim() + "' ";
            sql += ", supervisorFaena = '" + DropDownList1.SelectedItem.Text + "', caractUtilerias = '" + TextBox3.Text.Trim() + "', equiposLevante = '" + TextBox4.Text.Trim() + "', nombreTrabajador = '" + DropDownList2.SelectedItem.Text + "' ";
            sql += ", funcionTrabajador = '" + TextBox5.Text.Trim() + "', descripcionAcc = '" + TextBox6.Text.Trim() + "', lesiones = '" + TextBox7.Text.Trim() + "', fuenteAcc = '" + TextBox8.Text.Trim() + "' ";
            sql += ", Agente = '" + TextBox9.Text.Trim() + "', tipo = '" + TextBox10.Text.Trim() + "', accionSubestandar = '" + TextBox11.Text.Trim() + "', condicionSubestandar = '" + TextBox12.Text.Trim() + "'  ";
            sql += ", factorPersonal = '" + TextBox13.Text.Trim() + "', factorTrabajo = '" + TextBox14.Text.Trim() + "', faltaControl = '" + DropDownList3.SelectedItem.Text + "', accionCorrectiva = '" + TextBox15.Text.Trim() + "'  ";
            sql += ", nombreUser = '" + Context.Session["User"].ToString() + "', cargoUser = '" + DropDownList4.SelectedItem.Text + "' ";
            sql += "where  id =   " + Context.Session["idRegistro"].ToString();
        
        }
        try
        {
            conexion.ExecuteSql(sql);
            //EnviarMensaje4();
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        //
        Context.Session["graba"] = "1";
        //Limpiar Formulario
        limpiar();
        ComboBox();
        ComboBoxControl();
        ComboBoxEmpleado();
        ComboBoxCargo();
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
    }

    //Grilla
    protected void Grid_Ejemplo(Object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            PaneldeBusqueda.Visible = false;
            PaneldeEdicion.Visible = true;
            Context.Session["idRegistro"] = e.Item.Cells[1].Text;
            //Limpiar Formulario
            ComboBox();
            ComboBoxControl();
            ComboBoxEmpleado();
            ComboBoxCargo();
            limpiar();
            CargarFormulario(e.Item.Cells[1].Text);
        }
    }

    //Metodos

    protected DataTable CargarFormulario(string a)
    //protected DataTable CargarFormulario()
    {
        //Carga Formulario 
        DataTable dt = new DataTable();
        string sql = "";
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
        try
        {
            sql = "select * from tbAccidente where id = " + a.ToString();
            dt = conexion.ListarQueryDt(sql);
            // ComboBox();
            // limpiar();
            foreach (DataRow dr in dt.Rows)
            {
                TxtFecha1.Text = dr["fechaAcc"].ToString().Trim();
                TextBox1.Text = dr["lugarOcurrencia"].ToString().Trim();
                TextBox2.Text = dr["laborRealizada"].ToString().Trim();
                DropDownList1.SelectedItem.Text = dr["supervisorFaena"].ToString();
                TextBox3.Text = dr["caractUtilerias"].ToString().Trim();
                TextBox4.Text = dr["equiposLevante"].ToString().Trim();
                DropDownList2.SelectedItem.Text = dr["nombreTrabajador"].ToString();
                TextBox5.Text = dr["funcionTrabajador"].ToString().Trim();
                TextBox6.Text = dr["descripcionAcc"].ToString().Trim();
                TextBox7.Text = dr["lesiones"].ToString().Trim();
                TextBox8.Text = dr["fuenteAcc"].ToString().Trim();
                TextBox9.Text = dr["Agente"].ToString().Trim();
                TextBox10.Text = dr["tipo"].ToString().Trim();
                TextBox11.Text = dr["accionSubestandar"].ToString().Trim();
                TextBox12.Text = dr["condicionSubestandar"].ToString().Trim();
                TextBox13.Text = dr["factorPersonal"].ToString().Trim();
                TextBox14.Text = dr["factorTrabajo"].ToString().Trim();
                DropDownList3.SelectedItem.Text = dr["faltaControl"].ToString();
                TextBox15.Text = dr["accionCorrectiva"].ToString().Trim();
                //txtFecha2.Text = dr["fechaCorrectiva"].ToString().Trim();
                //txtFecha3.Text = dr["fechaVerificacion"].ToString().Trim();
                //txtFecha4.Text = dr["fechaCierre"].ToString().Trim();
                DropDownList4.SelectedItem.Text = dr["cargoUser"].ToString();

            }
            Context.Session["graba"] = "2";
        }
        catch (Exception Ex)
        {

            this.Error1.Text = Ex.Message;

        }
        return dt;
    }

    //Combo Encargado faena
    protected int ComboBox()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select id, nombreTrabajador+' '+apellidoTrabajador as [Nombre]  from tbTrabajador";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList1.Items.Clear();
        DropDownList1.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList1.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //Combo empleado
    protected int ComboBoxEmpleado()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select idPe, nombrePe+' '+ApePaternoPe as [Nombre]  from tbPersonal";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList2.Items.Clear();
        DropDownList2.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList2.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //Combo control
    protected int ComboBoxControl()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select * from TablaPrueba";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList3.Items.Clear();
        DropDownList3.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList3.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //Combo cargo
    protected int ComboBoxCargo()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select id, funcionTrabajador as [Nombre]  from tbTrabajador";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList4.Items.Clear();
        DropDownList4.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList4.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }
    //
    protected int limpiar()
    {
        ComboBox();
        int a = 0;
        TxtFecha1.Text = "";
        for (int i = 1; i <= 15; i++)
        {
            ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = "";
        }

        for (int i = 1; i <= 4; i++)
        {
            ((DropDownList)this.PaneldeEdicion.FindControl("DropDownList" + i.ToString())).SelectedValue = "0";
        }

        return a;
    }

    protected int ValidadordeCaracteresEspeciales()
    {
        int a = 0;

        return a;
    }

    protected int limpiarFormularioBusqueda()
    {
        //Limpia el Formulario de Busqueda
        txtBusqueda.Text = null;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        return 1;
    }

    //Calendarios
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        TxtFecha1.Text = Calendar1.SelectedDate.ToString("dd-MM-yyyy");
        Calendar1.Visible = true;
    }
}