﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ClosedUP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Context.Session.Clear();
        Response.Redirect("Default.aspx");
    }
}