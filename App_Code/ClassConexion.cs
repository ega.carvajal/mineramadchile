﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Descripción breve de ClassConexion
/// </summary>
public class ClassConexion
{
	public ClassConexion()
	{
		//
		// TODO: Agregar aquí la lógica del constructor
		//
	}

    public string conection = "Data Source=" + Environment.MachineName.ToString() + ";Initial Catalog=BD_MAD;Persist Security Info=True;User ID=mad;Password=1234$mad";

    public DataTable ListarQueryDt(string paramsql)
    {
        DataTable dt = new DataTable();
        try
        {
            using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(conection))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(paramsql, con);
                da.Fill(dt);
            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);

        }
        return dt;
    }

    public int ExecuteSql(string SQLString)
    {

        using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conection))
        {
            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(SQLString, connection))
            {
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (System.Data.SqlClient.SqlException e)
                {
                    connection.Close();
                    throw e;
                }

            }
        }
    }

    /* Ejemplo de llamado de procedure 
    protected DataTable prc_IngresarCasoBase()
    {
        DataTable dt = new DataTable();
        using (SqlConnection cnn = new SqlConnection(conn.conection))
        {
            cnn.Open();
            using (SqlCommand cmd = new SqlCommand("dbo.[prc_IngresarCasoBase]", cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 999999;

                cmd.Parameters.AddWithValue("@_Codigo", 0);// txtCodigo.Text);
                cmd.Parameters.AddWithValue("@_NuevoRemanente", txtTipoInversion.Text);
                cmd.Parameters.AddWithValue("@_NombredelaInversión", txtNombreInversiones.Text);
                cmd.Parameters.AddWithValue("@_GerenciaCliente", DropGerenciaCliente.SelectedValue);
                cmd.Parameters.AddWithValue("@_SuperintendenciaCliente", DropSICliente.SelectedValue);
                cmd.Parameters.AddWithValue("@_GerenciaEjecutor", DropGerenciaEjecutora.SelectedValue);
                cmd.Parameters.AddWithValue("@_CIMLP", TextBox5.Text);
                cmd.Parameters.AddWithValue("@_DirMLP", TextBox6.Text);
                cmd.Parameters.AddWithValue("@_DirAplc", TextBox7.Text);
                cmd.Parameters.AddWithValue("@_Termino", TextBox8.Text);
                cmd.Parameters.AddWithValue("@_Activo", DropDownListActivo.SelectedValue);
                cmd.Parameters.AddWithValue("@_Usuario", Context.Session["Nombre$_$Usuario"].ToString());

                SqlDataAdapter SqlDa = new SqlDataAdapter(cmd);
                SqlDa.SelectCommand.CommandTimeout = 999999;
                SqlDa.Fill(dt);
                SqlDa.SelectCommand.Cancel();
                cmd.Cancel();
            }
            cnn.Close();
        }
        return dt;
    }
     */

}