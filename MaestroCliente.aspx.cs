﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;

public partial class MaestroCliente : System.Web.UI.Page
{
    //private string lineaConexion = null;
    protected ClassConexion conexion = new ClassConexion();
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        //Load de la Pagina
        if (!IsPostBack)
        {
            Context.Session["idRegistro"] = "";
            Context.Session["graba"] = "";
            PaneldeBusqueda.Visible = true;
            PaneldeEdicion.Visible = false;
            
            //Inicio Copia
            try
            {
                //Paneles Seguridad
                //Buscar
                Button1.Visible = false;
                //Grabar
                Button2.Visible = false;
                //Volver
                Button3.Visible = false;
                //Nuevo Registro
                Button4.Visible = false;

                string sql = null;
                //string sql = "";
                sql = "Select top 1 * from tbPaginaPermiso where UsuarioCorre = '" + Context.Session["User"].ToString() + "' and DescripcionPaginaURL = 'MaestroCliente.aspx' and EstadoActivo = 'Activado'";
                //
                DataTable dt = new DataTable();
                dt = conexion.ListarQueryDt(sql);

                if (dt.Rows.Count == 0)
                {
                    Response.Redirect("CloseUP.aspx");
                }

                string TipoUsuario = "";

                foreach (DataRow ds in dt.Rows)
                {
                    TipoUsuario = ds[5].ToString().ToUpper();
                }

                switch (TipoUsuario)
                {
                    case "ADMINISTRADOR":
                        Button1.Visible = true;
                        Button2.Visible = true;
                        Button3.Visible = true;
                        Button4.Visible = true;
                        break;
                    default:
                        Response.Redirect("CloseUP.aspx");
                        break;
                }

            }
            catch (Exception ex)
            {
                Response.Redirect("ClosedUP.aspx");
            }

            //
        }
    }

    //
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Buscar y carga datagrid
        string sql = null;
        DataTable dt = new DataTable();
        //conexion a la base de datos y comando de carga del datagrib
        try
        {
            //sql = "select * from tbCliente ";
            sql = sql = "select cl.IdCl as ID, cl.Nombrecl as Nombre, cl.ApePaternoCl as 'Apellido Paterno', Cl.ApeMaternoCl as 'Apellido Materno', Cl.CelularCl as Celular, Cl.TelefonoCl as Telefono, Cl.CorreoUnoCl as 'Correo Personal', Cl.CorreoDosCl as 'Correo Compañia', cl.Direccion1 as 'Direccion Particular', cl.Direccion2 as 'Direccion del trabajo',  em.Descripcion 'Empresa', ea.Descripcion as 'Estado de Actividad'  from tbCliente cl inner join tbEmpresa em ON cl.TipoEmpresa = em.id inner join tbEstadoActivo ea ON cl.EstadoActivo=ea.id";
            if (txtBusqueda.Text != "")
            {
                sql += " where NombreCl like '%" + txtBusqueda.Text + "%'";
            }
            //Cargando Datagrid
            try
            {
                dt = conexion.ListarQueryDt(sql);
            }
            catch (Exception ex)
            {
                Error1.Text += ex.Message;
            }

            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            //Mensaje de Error desde la Base de datos.
            Error1.Text = Ex.Message;
        }
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Context.Session["graba"] = "1";
        //Limpiar y Cargar Combox
        //DropDownList1.Items.Clear();
        ComboBox();
        ComboBoxEstadoActivo();
        //Limpiar Formulario
        limpiar();
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
    }

    //IdClEmp
    protected void Button2_Click(object sender, EventArgs e)
    {
        ValidadordeCaracteresEspeciales();
        string sql = null;
        string EvaluaciondeFUncion = Context.Session["graba"].ToString();
        if (EvaluaciondeFUncion.ToString() == "1")
        {
            sql = " insert into [tbCliente] (IdCl, NombreCl, ApePaternoCl, ApeMaternoCl, CelularCl, TelefonoCl, CorreoUnoCl, CorreoDosCl, Direccion1, Direccion2, TipoEmpresa, EstadoActivo) values (( select isnull(max(IdCl),0) + 1 from  [tbCliente] ) , '" + TextBox1.Text.Trim() + "', '" + TextBox2.Text.Trim() + "', '" + TextBox3.Text.Trim() + "', '" + TextBox4.Text.Trim() + "', '" + TextBox5.Text.Trim() + "', '" + TextBox6.Text.Trim() + "', '" + TextBox7.Text.Trim() + "', '" + TextBox8.Text.Trim() + "', '" + TextBox9.Text.Trim() + "', '" + DropDownList2.SelectedValue + "','" + DropDownList1.SelectedValue + "' )"; //,'" + DropDownList1.SelectedValue + "' EstadoActivo

        }
        if (EvaluaciondeFUncion.ToString() == "2")
        {
            sql = " update [tbCliente] set NombreCl = '" + TextBox1.Text.Trim() + "'  , ApePaternoCl = '" + TextBox2.Text.Trim() + "', ApeMaternoCl = '" + TextBox3.Text.Trim() + "', CelularCl = '" + TextBox4.Text.Trim() + "', TelefonoCl = '" + TextBox5.Text.Trim() + "', CorreoUnoCl = '" + TextBox6.Text.Trim() + "',  CorreoDosCl = '" + TextBox7.Text.Trim() + "', Direccion1 = '" + TextBox8.Text.Trim() + "', Direccion2 = '" + TextBox9.Text.Trim() + "', TipoEmpresa= '" + DropDownList2.SelectedValue + "', EstadoActivo= '" + DropDownList1.SelectedValue + "' where  IdCl =   " + Context.Session["idRegistro"].ToString(); //EstadoActivo= '" + DropDownList1.SelectedValue + "'
        }

        try
        {
            conexion.ExecuteSql(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }

    //
    protected void Button3_Click(object sender, EventArgs e)
    {
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }

    //Grilla
    protected void Grid_Ejemplo(Object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            PaneldeBusqueda.Visible = false;
            PaneldeEdicion.Visible = true;
            Context.Session["idRegistro"] = e.Item.Cells[1].Text;
            //Limpiar y Cargar Combox
            //Limpiar Formulario
            DropDownList2.Items.Clear();
            DropDownList1.Items.Clear();
            ComboBox();
            ComboBoxEstadoActivo();
            limpiar();
            CargarFormulario(e.Item.Cells[1].Text);
        }
    }

    //Metodos

    protected DataTable CargarFormulario(string a)
    //protected DataTable CargarFormulario()
    {
        //Carga Formulario 
        DataTable dt = new DataTable();
        string sql = "";
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
        try
        {
            sql = "select * from tbCliente where [IdCl] = " + a.ToString();
            dt = conexion.ListarQueryDt(sql);
            // ComboBox();
            // limpiar();
            foreach (DataRow dr in dt.Rows)
            {
                TextBox1.Text = dr[1].ToString().Trim();
                TextBox2.Text = dr[2].ToString().Trim();
                TextBox3.Text = dr[3].ToString().Trim();
                TextBox4.Text = dr[4].ToString().Trim();
                TextBox5.Text = dr[5].ToString().Trim();
                TextBox6.Text = dr[6].ToString().Trim();
                TextBox7.Text = dr[7].ToString().Trim();
                TextBox8.Text = dr[8].ToString().Trim();
                TextBox9.Text = dr[9].ToString().Trim();
                DropDownList2.SelectedValue = dr["TipoEmpresa"].ToString();
                DropDownList1.SelectedValue = dr["EstadoActivo"].ToString();
            }
            Context.Session["graba"] = "2";
        }
        catch (Exception Ex)
        {

            this.Error1.Text = Ex.Message;

        }
        return dt;
    }

    //
    protected int ComboBox()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select id, Descripcion  from tbEmpresa";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList2.Items.Clear();
        DropDownList2.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList2.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //
    protected int ComboBoxEstadoActivo()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select  id, descripcion from tbestadoactivo ";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        DropDownList1.Items.Clear();
        DropDownList1.Items.Add(new ListItem("Seleccione", "0"));

        foreach (DataRow dr in dt.Rows)
        {
            DropDownList1.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //
    protected int limpiar()
    {
        ComboBox();
        ComboBoxEstadoActivo();
        int a = 0;
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        TextBox5.Text = "";
        TextBox6.Text = "";
        TextBox7.Text = "";
        TextBox8.Text = "";
        TextBox9.Text = "";
        DropDownList2.SelectedValue = "0";
        DropDownList1.SelectedValue = "0";
        return a;
    }

    protected int ValidadordeCaracteresEspeciales()
    {
        int a = 0;
        //Quita Valores Especiales del texbox
        //for (int i = 1; i <= 2; i++)
        //{
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("'", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¡", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¿", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("!", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("?", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(",", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(".", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("´", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("`", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(";", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("+", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("-", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("<", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(">", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("=", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("&", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("%", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("$", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("#", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("/", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Trim();
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.ToUpper();
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("SELECT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DELETE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("UPDATE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("INSERT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("ALL ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("TABLE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DROP ", "");

        //    if (((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text == "")
        //    {
        //        ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = "Sin Datos";
        //    }
        //}
        ////llena con un cero
        //if (TextBox3.Text == "")
        //{
        //    TextBox3.Text = "0";
        //}
        return a;
    }

    protected int limpiarFormularioBusqueda()
    {
        //Limpia el Formulario de Busqueda
        txtBusqueda.Text = null;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        return 1;
    }
    //
}