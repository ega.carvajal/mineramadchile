﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;


public partial class MaestroEmpresa : System.Web.UI.Page
{
    private string lineaConexion = null;

    protected ClassConexion conexion = new ClassConexion();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Load de la Pagina
        

        if (!IsPostBack) 
        {
            Context.Session["idRegistro"] = "";
            Context.Session["graba"] = "";
            PaneldeBusqueda.Visible = true;
            PaneldeEdicion.Visible = false;
            //
            try
            {
                //Paneles Seguridad
                //Buscar
                Button1.Visible = false;
                //Grabar
                Button2.Visible = false;
                //Volver
                Button3.Visible = false;
                //Nuevo Registro
                Button4.Visible = false;

                string sql = null;
                //string sql = "";
                sql = "Select top 1 * from tbPaginaPermiso where UsuarioCorre = '" + Context.Session["User"].ToString() + "' and DescripcionPaginaURL = 'MaestroEmpresa.aspx' and EstadoActivo = 'Activado'";
                //
                DataTable dt = new DataTable();
                dt = conexion.ListarQueryDt(sql);

                if (dt.Rows.Count == 0)
                {
                    Response.Redirect("CloseUP.aspx");
                }

                string TipoUsuario = "";

                foreach (DataRow ds in dt.Rows)
                {
                    TipoUsuario = ds[5].ToString().ToUpper();
                }

                switch (TipoUsuario)
                {
                    case "ADMINISTRADOR":
                        Button1.Visible = true;
                        Button2.Visible = true;
                        Button3.Visible = true;
                        Button4.Visible = true;
                        break;
                    default:
                        Response.Redirect("CloseUP.aspx");
                        break;
                }

            }
            catch (Exception ex)
            {
                Response.Redirect("ClosedUP.aspx");
            }
        }
        //variable de session
        //Context.Session["idRegistro"] -> esta almacena el Id del registro de la base de datos
        //Context.Session["grabar"] = 1 -> estado de un nuevo registro
        //Context.Session["grabar"] = 2 -> estado para modificar un registro
    }


    
   

    protected void Button1_Click(object sender, EventArgs e)
    {

        //Buscar y carga datagrid
        string sql = null;
        DataTable dt = new DataTable();
        //conexion a la base de datos y comando de carga del datagrib
        try
        {
            sql = " Select id as id, descripcion as [Descripción] , case when isnull(EstadoActivo,0) = 1 then 'Si' else 'No' end as Activo  from tbEmpresa ";
            if (txtBusqueda.Text != "")
            {
                sql += " where descripcion like '" + txtBusqueda.Text + "%'";
            }
            //Cargando Datagrid

            try
            {
                dt = conexion.ListarQueryDt(sql);
            }
            catch (Exception ex)
            {
                Error1.Text += ex.Message;
            }

            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            //Mensaje de Error desde la Base de datos.
            Error1.Text = Ex.Message;
        }
    }
    protected void Grid_Ejemplo(Object sender, DataGridCommandEventArgs e)
    {
        //selecciona el campo Id de la data Grid y realiza la busqueda del registro para cargarlo en el formulario 
        if (e.CommandName == "Select")
        {
            PaneldeBusqueda.Visible = false;
            PaneldeEdicion.Visible = true;
            Context.Session["idRegistro"] = e.Item.Cells[1].Text;
            //Limpiar y Cargar Combox
            
            //Limpiar Formulario
            limpiar();
            CargarFormulario(e.Item.Cells[1].Text);

        }
    }
    protected DataTable CargarFormulario(string a)
    {
        //Carga Formulario 
        DataTable dt = new DataTable();
        string sql = "";
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;

        try
        {
            sql = "Select * from tbempresa where id=  '" + a.ToString() + "'";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {

            Label1.Text = Ex.Message;

        }
        foreach (DataRow dr in dt.Rows)
        {

            TextBox1.Text = dr["Descripcion"].ToString();             
            DropDownList1.SelectedValue = dr["EstadoActivo"].ToString();

        }
        Context.Session["graba"] = "2";
        return dt;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        ValidadordeCaracteresEspeciales();
        string sql = null;
        string EvaluaciondeFUncion = Context.Session["graba"].ToString();
        if (EvaluaciondeFUncion.ToString() == "1")
        {
            sql = " insert into tbEmpresa (id,Descripcion,EstadoActivo) values (( select isnull(max(id),0) + 1 from  tbEmpresa ) , '" + TextBox1.Text + "', '" + DropDownList1.SelectedValue + "'    ) ";
        }
        if (EvaluaciondeFUncion.ToString() == "2")
        {
            sql = " update tbEmpresa set Descripcion = '" + TextBox1.Text + "'  , EstadoActivo= '" + DropDownList1.SelectedValue + "'  where  id =   " + Context.Session["idRegistro"].ToString();
        }

        try
        {
             conexion. ExecuteSql(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        //Error2.Text = sql;
        //Error1.Text = sql;       

        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }
    protected int ComboBox()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select  *   from tabladePrueba ";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        DropDownList1.Items.Add(new ListItem("Seleccione", "0"));

        foreach (DataRow dr in dt.Rows)
        {
            DropDownList1.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Context.Session["graba"] = "1";
        //Limpiar y Cargar Combox
        //DropDownList1.Items.Clear();
        //ComboBox();
        //Limpiar Formulario
        limpiar();
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
    }

    protected void Button3_Click(object sender, EventArgs e)
    {

        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }
    protected int limpiar()
    {
        int a = 0;
        //limpiar texbox
        //for (int i = 1; i <= 3; i++)
        //{
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = "";
        //}

        //-----
        //limpiar ComboBox
        TextBox1.Text = "";
        DropDownList1.SelectedValue = "0";
        return a;
    }
    protected int ValidadordeCaracteresEspeciales()
    {
        int a = 0;
        //Quita Valores Especiales del texbox
        //for (int i = 1; i <= 2; i++)
        //{
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("'", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¡", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¿", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("!", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("?", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(",", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(".", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("´", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("`", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(";", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("+", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("-", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("<", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(">", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("=", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("&", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("%", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("$", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("#", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("/", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Trim();
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.ToUpper();
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("SELECT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DELETE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("UPDATE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("INSERT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("ALL ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("TABLE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DROP ", "");

        //    if (((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text == "")
        //    {
        //        ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = "Sin Datos";
        //    }
        //}
        ////llena con un cero
        //if (TextBox3.Text == "")
        //{
        //    TextBox3.Text = "0";
        //}
        return a;
    }


    protected int limpiarFormularioBusqueda()
    {
        //Limpia el Formulario de Busqueda
        txtBusqueda.Text = null;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        return 1;
    }
}