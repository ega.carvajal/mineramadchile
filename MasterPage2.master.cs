﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage2 : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Context.Session["user"] == null)
                {
                    PanelSignUp.Visible = true;
                    PanelSistema.Visible = false;
                }
                else
                {
                    txtNombre.Text = Context.Session["NombreUsuario"].ToString();
                    txtTipo.Text ="Perfil : " +  Context.Session["Tipo$_$Usuario"].ToString();

                    PanelSignUp.Visible = false;
                    PanelSistema.Visible = true;
                    PanelAdministrador.Visible = false;
                    PanelProyectos.Visible = false;
                    PanelSeguridad.Visible = false;
                    PanelCompraVenta.Visible = false;
                    switch (Context.Session["Tipo$_$Usuario"].ToString().ToUpper())
                    {
                        case "ADMINISTRADOR":
                            PanelAdministrador.Visible = true;
                            //testing
                            //PanelCompraVenta.Visible = true;
                            break;
                        case "SUPERUSUARIO":
                            PanelProyectos.Visible = true;
                            PanelSeguridad.Visible = true;
                            PanelCompraVenta.Visible = true;
                            //Testing
                            PanelAdministrador.Visible = false;
                            break;
                        case "LECTURA":
                            PanelProyectos.Visible = true;
                            PanelCompraVenta.Visible = true;
                            break;
                        case "EDITOR":
                            PanelProyectos.Visible = true;
                            PanelCompraVenta.Visible = false;
                            break;
                        case "DIOS":
                            PanelAdministrador.Visible = true;
                            PanelProyectos.Visible = true;
                            PanelSeguridad.Visible = true;
                            PanelCompraVenta.Visible = true;
                            break;
                        default:
                            Response.Redirect("CloseUP.aspx");
                            break;
                    }






                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Default.aspx");
            }
        }
    }
}
