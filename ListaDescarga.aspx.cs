﻿using System;
using System.Collections.Generic;
 
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
  



public partial class ListaDescarga : System.Web.UI.Page
{
    protected ClassConexion conn = new ClassConexion();

    protected void Page_Load(object sender, EventArgs e)
    {
        string usuario = "";
        string password = "";
        string llave = "";
        Context.Session.Timeout = 1024;
        if (!IsPostBack)
        {
            try
            {
                if (Context.Session["User"].ToString() != "" || Context.Session["User"] != null)
                {


                    usuario = Context.Session["User"].ToString();
                    password = Context.Session["Nombre$_$Usuario"].ToString();


                    if (Context.Session["dtAnterior"] != null)
                    {
                        DataTable dt = new DataTable();
                        dt = Context.Session["dtAnterior"] as DataTable;



                        Context.Session.Timeout = 1024;
                        if (!IsPostBack)
                        {
                            try
                            {

                                GridView1.DataSource = dt;
                                GridView1.DataBind();
                            }
                            catch (Exception ex)
                            {

                            }

                        }


                    }


                  
                }
            }
            catch (Exception ex)
            {                
                Response.Write(ex.Message);
            }
        }

    }
    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        if (Di.SelectedValue == "1")
        {
            HttpResponse response = Response;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pageToRender = new Page();
            HtmlForm form = new HtmlForm();
            form.Controls.Add(GridView1);
            pageToRender.Controls.Add(form);
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/ms-excel";
            //response.AddHeader("Content-Disposition", "attachment;filename=" + "Archivo.xls");
            response.AddHeader("Content-Disposition", "attachment;filename=Documento.xls");
            response.Charset = "UTF-8";
            response.ContentEncoding = Encoding.Default;
            pageToRender.RenderControl(htw);
            response.Write(sw.ToString());
            response.End();
        }
        if (Di.SelectedValue == "2")
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    //To Export all pages
                    GridView1.AllowPaging = false;
                   
                    GridView1.RenderControl(hw);
                    StringReader sr = new StringReader(sw.ToString());
                    Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    htmlparser.Parse(sr);
                    pdfDoc.Close();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=Documento.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                }
            }

        }

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
         
    }
}