﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;

public partial class MaestroEncargado : System.Web.UI.Page
{
    protected ClassConexion conexion = new ClassConexion();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Load de la Pagina
        if (!IsPostBack)
        {
            Context.Session["idRegistro"] = "";
            Context.Session["graba"] = "";
            PaneldeBusqueda.Visible = true;
            PaneldeEdicion.Visible = false;
            //
            try
            {
                //Paneles Seguridad
                //Buscar
                Button1.Visible = false;
                //Grabar
                Button2.Visible = false;
                //Volver
                Button3.Visible = false;
                //Nuevo Registro
                Button4.Visible = false;

                string sql = null;
                //string sql = "";
                sql = "Select top 1 * from tbPaginaPermiso where UsuarioCorre = '" + Context.Session["User"].ToString() + "' and DescripcionPaginaURL = 'MaestroEncargado.aspx' and EstadoActivo = 'Activado'";
                //
                DataTable dt = new DataTable();
                dt = conexion.ListarQueryDt(sql);

                if (dt.Rows.Count == 0)
                {
                    Response.Redirect("CloseUP.aspx");
                }

                string TipoUsuario = "";

                foreach (DataRow ds in dt.Rows)
                {
                    TipoUsuario = ds[5].ToString().ToUpper();
                }

                switch (TipoUsuario)
                {
                    case "ADMINISTRADOR":
                        Button1.Visible = true;
                        Button2.Visible = true;
                        Button3.Visible = true;
                        Button4.Visible = true;
                        break;
                    default:
                        Response.Redirect("CloseUP.aspx");
                        break;
                }

            }
            catch (Exception ex)
            {
                Response.Redirect("ClosedUP.aspx");
            }
        }
        //variable de session
        //Context.Session["idRegistro"] -> esta almacena el Id del registro de la base de datos
        //Context.Session["grabar"] = 1 -> estado de un nuevo registro
        //Context.Session["grabar"] = 2 -> estado para modificar un registro
    }

    //
    protected void Button1_Click(object sender, EventArgs e)
    {
        Error1.Text = "";
        Error2.Text = "";
        //Buscar y carga datagrid
        string sql = null;
        DataTable dt = new DataTable();
        //conexion a la base de datos y comando de carga del datagrib
        try
        {
            //sql = " execute SP_ListaTipoUs";
            //sql = "select * from TbPersonal"; //case when isnull(EstadoActivo,0) = 1 then 'Si' else 'No' end as Activo
            sql = "select * from tbTrabajador";
            if (txtBusqueda.Text != "")
            {
                sql += " where NombreTrabajador like '%" + txtBusqueda.Text + "%'";
            }
            //Cargando Datagrid
            try
            {
                dt = conexion.ListarQueryDt(sql);
            }
            catch (Exception ex)
            {
                Error1.Text += ex.Message;
            }

            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            //Mensaje de Error desde la Base de datos.
            Error1.Text = Ex.Message;
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        ValidadordeCaracteresEspeciales();
        string sql = null;
        string EvaluaciondeFUncion = Context.Session["graba"].ToString();
        if (EvaluaciondeFUncion.ToString() == "1")
        {
            sql = " insert into [TbTrabajador] (id, nombreTrabajador, apellidoTrabajador, rutTrabajador, edadTrabajador, funcionTrabajador, annosContrata, claseContrato, estadoCivil, domicilioTrabajador, estadoActivo) values (( select isnull(max(id),0) + 1 from  [TbTrabajador] ) , '" + TextBox1.Text.Trim() + "', '" + TextBox2.Text.Trim() + "', '" + TextBox3.Text.Trim() + "', '" + TextBox4.Text.Trim() + "', '" + TextBox5.Text.Trim() + "','" + TextBox6.Text.Trim() + "' , '" + DropDownList1.SelectedItem.Text + "','" + DropDownList2.SelectedItem.Text + "' ,'" + TextBox7.Text.Trim() + "','" + DropDownList3.SelectedItem.Text + "')";
            //, Descripcion, EstadoActivo
        }
        if (EvaluaciondeFUncion.ToString() == "2")
        {
            sql = " update [TbTrabajador] set nombreTrabajador = '" + TextBox1.Text.Trim() + "', apellidoTrabajador = '" + TextBox2.Text.Trim() + "', rutTrabajador = '" + TextBox3.Text.Trim() + "', edadTrabajador = '" + TextBox4.Text.Trim() + "', funcionTrabajador = '" + TextBox5.Text.Trim() + "', annosContrata= '" + TextBox6.Text.Trim() + "', claseContrato = '" + DropDownList1.SelectedItem.Text + "', estadoCivil = '" + DropDownList2.SelectedItem.Text + "', domicilioTrabajador = '" + TextBox7.Text.Trim() + "', estadoActivo = '" + DropDownList3.SelectedItem.Text + "'  where  id =   " + Context.Session["idRegistro"].ToString();
        }

        try
        {
            conexion.ExecuteSql(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        //Error2.Text = sql;
        //Error1.Text = sql;       
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Context.Session["graba"] = "1";
        //Limpiar y Cargar Combox
        DropDownList1.Items.Clear();
        DropDownList2.Items.Clear();
        DropDownList3.Items.Clear();
        ComboBox();
        ComboBoxCivil();
        ComboBoxEstado();
        //Limpiar Formulario
        limpiar();
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
    }

    //

    //Grilla
    protected void Grid_Ejemplo(Object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            PaneldeBusqueda.Visible = false;
            PaneldeEdicion.Visible = true;
            Context.Session["idRegistro"] = e.Item.Cells[1].Text;
            //Limpiar y Cargar Combox
            //Limpiar Formulario
            DropDownList1.Items.Clear();
            DropDownList2.Items.Clear();
            DropDownList3.Items.Clear();
            ComboBox();
            ComboBoxCivil();
            ComboBoxEstado();
            limpiar();
            CargarFormulario(e.Item.Cells[1].Text);
        }
    }

    //Metodos
    protected DataTable CargarFormulario(string a)
    //protected DataTable CargarFormulario()
    {
        //Carga Formulario 
        DataTable dt = new DataTable();
        string sql = "";
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
        try
        {
            sql = "select * from tbTrabajador where id = " + a.ToString();
            dt = conexion.ListarQueryDt(sql);
            // ComboBox();
            // limpiar();
            foreach (DataRow dr in dt.Rows)
            {
                TextBox1.Text = dr["nombreTrabajador"].ToString().Trim();
                TextBox2.Text = dr["apellidoTrabajador"].ToString().Trim();
                TextBox3.Text = dr["rutTrabajador"].ToString().Trim();
                TextBox4.Text = dr["edadTrabajador"].ToString().Trim();
                TextBox5.Text = dr["funcionTrabajador"].ToString().Trim();
                TextBox6.Text = dr["annosContrata"].ToString().Trim();
                DropDownList1.SelectedItem.Text = dr["claseContrato"].ToString();
                DropDownList2.SelectedItem.Text = dr["estadoCivil"].ToString();
                TextBox7.Text = dr["domicilioTrabajador"].ToString().Trim();
                DropDownList3.SelectedItem.Text = dr["estadoActivo"].ToString();
            }
            Context.Session["graba"] = "2";
        }
        catch (Exception Ex)
        {
            this.Error1.Text = Ex.Message;
        }
        return dt;
    }

    //Contrato
    protected int ComboBox()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select idContrato, DescripcionContrato  from tbTipoContrato";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList1.Items.Clear();
        DropDownList1.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList1.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //EstadoCivil
    protected int ComboBoxCivil()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select id, descripcion  from tbEstadoCivil";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList2.Items.Clear();
        DropDownList2.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList2.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //EstadoActivo
    protected int ComboBoxEstado()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select id, Descripcion  from tbEstadoActivo";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList3.Items.Clear();
        DropDownList3.Items.Add(new ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList3.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //
    protected int limpiar()
    {
        ComboBox();
        ComboBoxCivil();
        ComboBoxEstado();
        int a = 0;
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        TextBox5.Text = "";
        TextBox6.Text = "";
        TextBox7.Text = "";
        DropDownList1.SelectedValue = "0";
        DropDownList2.SelectedValue = "0";
        DropDownList2.SelectedValue = "0";
        return a;
    }

    protected int ValidadordeCaracteresEspeciales()
    {
        int a = 0;
        return a;
    }

    protected int limpiarFormularioBusqueda()
    {
        //Limpia el Formulario de Busqueda
        txtBusqueda.Text = null;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        return 1;
    }

    //
}