﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
//
using System.IO;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html;
//
 
using System.Threading;
using System.Net.Mail;
using System.Net;


public partial class MaestroIngresoProyecto : System.Web.UI.Page
{
    protected ClassConexion conexion = new ClassConexion();
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        //Load de la Pagina
        if (!IsPostBack)
        {
            //System.Web.HttpContext.Current.Response.Write(Context.Session["User"].ToString());
            //
            Context.Session["idRegistro"] = "";
            Context.Session["graba"] = "";
            ComboBox();
            ComboBoxCliente();
            ComboBoxEmp();
            ComboBoxPersonal();
            ComboBoxEjecucion1();
            ComboBoxEjecucion2();
            ComboBoxEstatus();
            ComboBoxOpciones();
            //
            //Inicio Copia
            btnPdf.Visible = false;
            btnPdf.Enabled = false;
            try
            {
                //Paneles Seguridad
                //Buscar
                Button1.Visible = false;
                //Grabar
                Button2.Visible = false;
                //Volver
                Button3.Visible = false;
                //Nuevo Registro
                Button4.Visible = false;
                
                string sql = null;
                //string sql = "";
                sql = "Select top 1 * from tbPaginaPermiso where UsuarioCorre = '" + Context.Session["User"].ToString() + "' and DescripcionPaginaURL = 'MaestroIngresoProyecto.aspx' and EstadoActivo = 'Activado'";
                                //
                DataTable dt = new DataTable();
                dt = conexion.ListarQueryDt(sql);

                if (dt.Rows.Count == 0)
                {
                    Response.Redirect("CloseUP.aspx");
                }

                string TipoUsuario = "";

                foreach (DataRow ds in dt.Rows)
                {
                    TipoUsuario = ds[5].ToString().ToUpper();
                }

                switch (TipoUsuario)
                //switch (TipoUsuarioo)
                //switch (Context.Session["Tipo$_$Usuario"].ToString().ToUpper())
                {
                    case "SUPERUSUARIO":
                        Button1.Visible = true;
                        Button2.Visible = true;
                        Button3.Visible = true;
                        Button4.Visible = true;
                        //btnPdf.Visible = true;
                        //btnCorreo.Visible = true;
                        break;
                    default:
                        Response.Redirect("CloseUP.aspx");
                        break;
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("ClosedUP.aspx");
                //Error1.Text = ex.Message;

            }
            //Coppia fin

            PaneldeBusqueda.Visible = true;
            PaneldeEdicion.Visible = false;
            //ComboBox();
        }
    }

    //Boton buscar
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Buscar y carga datagrid

        Error1.Text = "";
        Error2.Text = "";
        btnPdf.Visible = true;
        //btnCorreo.Visible = true;
        string sql = null;
        DataTable dt = new DataTable();
        //conexion a la base de datos y comando de carga del datagrid
        try
        {
            sql = "  select   ";
            sql += "   ip.id as [ID SISTEMA] ";
            sql += " , ip.mes as 'Fecha registro proyecto' ";
            sql += " , ip.descripcion as 'Descripción del proyecto' ";
            sql += " , (us.NombreUs+' '+us.ApePaternoUs) as 'Usuario del sistema' ";
            sql += " , (cl.NombreCl+' '+cl.ApePaternoCl) as Cliente  ";
            sql += " , em.Descripcion as Empresa ";
            sql += " , (pe.NombrePe+' '+pe.ApePaternoPe) as 'Personal a cargo' ";
            sql += " ,  op.descripcion as '24 horas'";
            sql += " , case when adjudicacion = 1 then 'Si' else 'No' end      as  'Adjudicación' ";
            sql += " ,  case when ing1 = 1 then 'Si' else 'No' end      as 'Ing. de 5 días' ";
            sql += " , tp.DescTp as 'Primera Ejecución' ";
            sql += " ,  case when ing2 = 1 then 'Si' else 'No' end   as  'Ing. inversa de 30 días' ";
            sql += " , obs as 'Obersación' ";
            sql += " , te.DescTe as 'Segundo ejecución' ";
            sql += " , case when ingreso = 1 then 'Si' else 'No' end  as 'Ingreso del proyecto'";
            sql += " , case when cierre = 1 then 'Si' else 'No' end  as 'Cierre  del proyecto' ";
            sql += " , case when entregable = 1 then 'Si' else 'No' end   as 'Entregables del proyecto'";
            sql += " , es.DescripcionEstatus as 'Estado del registro'";
            //
            //sql += " , (usM.NombreUs+' '+usM.ApePaternoUs) as [Usuario modificación]  ";
            //
            sql += ", ip.usuarioModificacion as [Usuario modificación]";
            //
            sql += " , ip.FechaModificacion as [Fecha Modificación]  ";
            
            //Context.Session["User"]

            sql += " from tbIngresoProyecto ip  ";
            sql += " left join tbUsuario us ON ip.usuario = us.idus  ";
            sql += " left join tbCliente cl ON ip.cliente = cl.IdCl  ";
            sql += " left join tbEmpresa em ON ip.empresa = em.id  ";
            sql += " left join tbPersonal pe ON ip.personal = pe.IdPe  ";
            sql += " left join tbTipoProceso tp ON ip.ejecucion1 = tp.IdTp  ";
            sql += " left join tbTipoEjecucion te ON ip.ejecucion2 = te.IdTe  ";
            sql += " left join tbOpciones op ON ip.horas = op.id";
            sql += " left join tbEstatus es ON ip.estatus = es.idEstatus ";
            sql += " left join tbUsuario usM ON ip.usuarioModificacion = usM.idus  ";

            //sql = "";
            if (txtBusqueda.Text.TrimEnd().TrimStart() != "")
            {
                sql += " where ip.descripcion like '%" + txtBusqueda.Text + "%' ";
                // sql += " and tipo = '" + txtBusqueda.Text + "' ";
            }
            //Cargando Datagrid
            sql += " group by  ";
            sql += " ip.id   ";
            sql += " ,ip.mes   ";
            sql += " ,ip.descripcion  ";
            sql += " ,us.NombreUs   ";
            sql += " ,us.ApePaternoUs ";
            sql += " ,cl.NombreCl ";
            sql += " ,cl.ApePaternoCl ";
            sql += " ,em.Descripcion   ";
            sql += " ,pe.NombrePe ";
            sql += " ,pe.ApePaternoPe ";
            sql += " ,op.descripcion  ";
            sql += " ,ip.adjudicacion   ";
            sql += " ,ip.ing1   ";
            sql += " ,tp.DescTp ";
            sql += " ,ip.ing2    ";
            sql += " ,ip.obs   ";
            sql += " ,te.DescTe ";
            sql += " ,ip.ingreso  ";
            sql += " ,ip.cierre  ";
            sql += " ,ip.entregable ";
            sql += " ,es.DescripcionEstatus  ";
            sql += " ,usM.NombreUs ";
            sql += " ,usM.ApePaternoUs   ";
            sql += " ,ip.FechaModificacion   ";
            //
            sql += ", ip.usuarioModificacion";

            try
            {
                dt = conexion.ListarQueryDt(sql);
                //Session para descarga de datos

                if (dt.Rows.Count>0 )
                {
                    btnPdf.Enabled = true;
                }

                Context.Session["dtAnterior"] = dt;
                //Context.Session["User"] = dt;
            }
            catch (Exception ex)
            {
                Error1.Text += ex.Message;
            }

            DataGrid1.DataSource = dt;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            //Mensaje de Error desde la Base de datos.
            Error1.Text = Ex.Message;
        }
    }

    //Boton grabar
    protected void Button2_Click(object sender, EventArgs e)
    {
        ValidadordeCaracteresEspeciales();
        string sql = null;
        string EvaluaciondeFUncion = Context.Session["graba"].ToString();
        if (EvaluaciondeFUncion.ToString() == "1")
        {
            sql = " insert into [tbIngresoProyecto] (id, mes, descripcion, usuario, cliente, empresa, personal, horas, adjudicacion, ing1, ejecucion1, ing2, obs, ejecucion2, ingreso, cierre, entregable, estatus, usuarioModificacion) values (( select isnull(max(id),0) + 1 from  [tbIngresoProyecto] ), (select GETDATE()) , '" + TextBox1.Text.Trim() + "', '" + DropDownList1.SelectedValue + "', '" + DropDownList2.SelectedValue + "', '" + DropDownList3.SelectedValue + "', '" + DropDownList4.SelectedValue + "', '" + DropDownList5.SelectedValue + "', '" + DropDownList6.SelectedValue + "', '" + DropDownList7.SelectedValue + "' , '" + DropDownList8.SelectedValue + "', '" + DropDownList9.SelectedValue + "', '" + TextBox2.Text.Trim() + "', '" + DropDownList10.SelectedValue + "', '" + DropDownList11.SelectedValue + "', '" + DropDownList12.SelectedValue + "', '" + DropDownList13.SelectedValue + "', '" + DropDownList14.SelectedValue + "', '" + Context.Session["User"].ToString() + "')";

        }
        if (EvaluaciondeFUncion.ToString() == "2")
        {
            sql = " update [tbIngresoProyecto] set  descripcion = '" + TextBox1.Text.Trim() + "', fechaModificacion = (select GETDATE()), usuario= '" + DropDownList1.SelectedValue + "' , cliente= '" + DropDownList2.SelectedValue + "', empresa='" + DropDownList3.SelectedValue + "', personal='" + DropDownList4.SelectedValue + "', horas = '" + DropDownList5.SelectedValue + "', adjudicacion = '" + DropDownList6.SelectedValue + "', ing1 = '" + DropDownList7.SelectedValue + "', ejecucion1 = '" + DropDownList8.SelectedValue + "', ing2 = '" + DropDownList9.SelectedValue + "', obs = '" + TextBox2.Text.Trim() + "', ejecucion2 = '" + DropDownList10.SelectedValue + "', ingreso = '" + DropDownList11.SelectedValue + "', cierre = '" + DropDownList12.SelectedValue + "', entregable = '" + DropDownList13.SelectedValue + "', estatus = '" + DropDownList14.SelectedValue + "', usuarioModificacion= '" + Context.Session["User"].ToString() + "' where  id =   " + Context.Session["idRegistro"].ToString();
        }

        try
        {
            conexion.ExecuteSql(sql);
            EnviarMensaje4();
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }

    protected void EnviarMensaje4()
    {
        SmtpClient cliente;
        MailMessage mm;

        string usuario = "";
        string contraseña = "";
        string servidor = "";
        int puerto = 0;

        string mail1, mail2;

        string etapa = "";
        DataTable dt = new DataTable();

        usuario = "minera.madchile@gmail.com";
        contraseña = "1234$mad";
        servidor = "smtp.gmail.com";
        puerto = 587;

        String cuerpoCompleto = "";
        cuerpoCompleto += " ID de Caso Base : <b> " + Context.Session["idRegistro"].ToString() + " </b><br>";
        cuerpoCompleto += " Titulo de Proyecto  : <b> " + TextBox1.Text + " </b><br>";
        cuerpoCompleto += "<br/>Comentario: <br/><p>Revisar registros en el sistema</p><br/><br/>";
        cuerpoCompleto += "<a href='http://lessons.mine.nu/desamad/'>Ir Sistema MAD</a>";

        //mm = new MailMessage(Context.Session["Usuario$Creador"].ToString(), Context.Session["Nombre$_$Usuario"].ToString(), etapa, cuerpoCompleto);
        //mm.IsBodyHtml = true;
        //mm.BodyEncoding = System.Text.UTF8Encoding.UTF8;
        //mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

        //cliente.Send(mm);

        try
        {
            //Conexión a a la Plataforma de Microsofot Office 365 para enviar correo.
            //var smtp = new System.Net.Mail.SmtpClient("outlook.office365.com");

            //En el caso que quiera conectarse a plataforma Gmail para enviar correo.
            var smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");

            var mail = new System.Net.Mail.MailMessage();
            string userFrom = usuario; //Mi cuenta de Gmail u Office 365.

            // IMPORTANTE : Este Usuario mail.From, debe coincidir con el de NetworkCredential(), sino se geenera error.
            mail.From = new System.Net.Mail.MailAddress(usuario, "MAD Proyectos");
            
            string correo = DropDownList4.SelectedItem.Text;
            string[] parts = correo.Split('|');
            string correo_aux = "";
            foreach (string part in parts)
            {
                correo_aux =part ;
            }
            correo_aux = parts[1].ToString().Trim();
            
            //Correos Destino a los que les enviaré mail.
            mail.To.Add(new System.Net.Mail.MailAddress(correo_aux));
            mail.CC.Add(new System.Net.Mail.MailAddress(Context.Session["User"].ToString()));

            mail.Subject = "Sistema MAD  / " + TextBox1.Text + " / ID Caso: " + Context.Session["idRegistro"].ToString();
            mail.Body = cuerpoCompleto;//"<b>" + etapa + "</b><br/><br/><p>" + this.txtComentario4.Text + "</p><br/><br/>";

            mail.IsBodyHtml = true;
            mail.BodyEncoding = System.Text.UTF8Encoding.UTF8;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;

            //Credenciales que se utilizan, cuando se autentica al correo de Gmail u Office 365.
            smtp.Credentials = new System.Net.NetworkCredential(usuario, contraseña);

            smtp.Send(mail);
        }
        catch (Exception ex)
        {
            this.Error2.Text = ex.Message;
        }
     }

    //Boton volver
    protected void Button3_Click(object sender, EventArgs e)
    {
        PaneldeBusqueda.Visible = true;
        PaneldeEdicion.Visible = false;
        limpiarFormularioBusqueda();
    }

    //Boton nuevo registro
    protected void Button4_Click(object sender, EventArgs e)
    {
        //
        Context.Session["graba"] = "1";
        //Limpiar y Cargar Combox
        ComboBox();
        ComboBoxCliente();
        ComboBoxEmp();
        ComboBoxPersonal();
        ComboBoxEjecucion1();
        ComboBoxEjecucion2();
        ComboBoxEstatus();
        ComboBoxOpciones();
        //Limpiar Formulario
        limpiar();
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
    }

    //
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    //Grilla
    protected void Grid_Ejemplo(Object sender, DataGridCommandEventArgs e)
    {
        //selecciona el campo Id de la data Grid y realiza la busqueda del registro para cargarlo en el formulario 
        if (e.CommandName == "Select")
        {
            PaneldeBusqueda.Visible = false;
            PaneldeEdicion.Visible = true;
            Context.Session["idRegistro"] = e.Item.Cells[1].Text;
            //Limpiar y Cargar Combox
            //Limpiar Formulario
            DropDownList1.Items.Clear();
            DropDownList2.Items.Clear();
            DropDownList3.Items.Clear();
            DropDownList4.Items.Clear();
            DropDownList5.Items.Clear();
            DropDownList6.Items.Clear();
            DropDownList7.Items.Clear();
            DropDownList8.Items.Clear();
            DropDownList9.Items.Clear();
            DropDownList10.Items.Clear();
            DropDownList11.Items.Clear();
            DropDownList12.Items.Clear();
            DropDownList13.Items.Clear();
            DropDownList14.Items.Clear();
            ComboBox();
            ComboBoxCliente();
            ComboBoxEmp();
            ComboBoxPersonal();
            ComboBoxEjecucion1();
            ComboBoxEjecucion2();
            ComboBoxEstatus();
            ComboBoxOpciones();
            CargarFormulario(e.Item.Cells[1].Text);
        }
    }

    //Metodos
    protected DataTable CargarFormulario(string a)
    //protected DataTable CargarFormulario()
    {
        //Carga Formulario 
        DataTable dt = new DataTable();
        string sql = "";
        PaneldeBusqueda.Visible = false;
        PaneldeEdicion.Visible = true;
        try
        {
            sql = "select * from tbIngresoProyecto where id = " + a.ToString(); //+ a.ToString()
            dt = conexion.ListarQueryDt(sql);

            // ComboBox();
            // limpiar();
            foreach (DataRow dr in dt.Rows)
            {
                label1.Text = dr["mes"].ToString().Trim();
                TextBox1.Text = dr["descripcion"].ToString().Trim();
                DropDownList1.SelectedValue = dr["usuario"].ToString();
                DropDownList2.SelectedValue = dr["cliente"].ToString();
                DropDownList3.SelectedValue = dr["empresa"].ToString();
                DropDownList4.SelectedValue = dr["personal"].ToString();
                DropDownList5.SelectedValue = dr["horas"].ToString();
                DropDownList6.SelectedValue = dr["adjudicacion"].ToString();
                DropDownList7.SelectedValue = dr["ing1"].ToString();
                DropDownList8.SelectedValue = dr["ejecucion1"].ToString();
                DropDownList9.SelectedValue = dr["ing2"].ToString();
                TextBox2.Text = dr["obs"].ToString().Trim();
                DropDownList10.SelectedValue = dr["ejecucion2"].ToString();
                DropDownList11.SelectedValue = dr["ingreso"].ToString();
                DropDownList12.SelectedValue = dr["cierre"].ToString();
                DropDownList13.SelectedValue = dr["entregable"].ToString();
                DropDownList14.SelectedValue = dr["estatus"].ToString();
            }
            Context.Session["graba"] = "2";
        }
        catch (Exception Ex)
        {

            this.Error1.Text = Ex.Message;
        }
        return dt;
    }

    //Usuario 
    protected int ComboBox()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select idus, NombreUs from tbUsuario";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        DropDownList1.Items.Clear();
        DropDownList1.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList1.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //Cliente
    protected int ComboBoxCliente()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select IdCl, NombreCl  from tbCliente";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList2.Items.Clear();
        DropDownList2.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList2.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));

        }

        return 1;
    }

    //Empresa
    protected int ComboBoxEmp()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select id, Descripcion from tbEmpresa";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList3.Items.Clear();
        DropDownList3.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList3.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));

        }

        return 1;
    }

    //Personal a cargo 
    protected int ComboBoxPersonal()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select idpe, nombrepe + ' | ' + CorreoUnoPe from tbPersonal";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }

        DropDownList4.Items.Clear();
        DropDownList4.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList4.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));

        }

        return 1;
    }

    //
    protected int ComboBoxOpciones()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select * from tbOpciones";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        DropDownList5.Items.Clear();
        DropDownList5.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        //
        DropDownList6.Items.Clear();
        DropDownList6.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        DropDownList7.Items.Clear();
        DropDownList7.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        DropDownList9.Items.Clear();
        DropDownList9.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        DropDownList11.Items.Clear();
        DropDownList11.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        DropDownList12.Items.Clear();
        DropDownList12.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        DropDownList13.Items.Clear();
        DropDownList13.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList5.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
            DropDownList6.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
            DropDownList7.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
            DropDownList9.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
            DropDownList11.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
            DropDownList12.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
            DropDownList13.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //Ejecucion 1
    protected int ComboBoxEjecucion1()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select * from tbTipoProceso";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        DropDownList8.Items.Clear();
        DropDownList8.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList8.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //Ejecucion 2
    protected int ComboBoxEjecucion2()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select * from tbTipoEjecucion";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        DropDownList10.Items.Clear();
        DropDownList10.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList10.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //Estatus
    protected int ComboBoxEstatus()
    {
        DataTable dt = new DataTable();
        string sql = "";
        try
        {
            sql = "select * from tbEstatus";
            dt = conexion.ListarQueryDt(sql);
        }
        catch (Exception Ex)
        {
            //mensajes de errores
            Error1.Text = Ex.Message;
            Error2.Text = Ex.Message;
        }
        DropDownList14.Items.Clear();
        DropDownList14.Items.Add(new System.Web.UI.WebControls.ListItem("Seleccione", "0"));
        foreach (DataRow dr in dt.Rows)
        {
            DropDownList14.Items.Add(new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString()));
        }

        return 1;
    }

    //
    protected int limpiar()
    {
        ComboBox();
        ComboBoxCliente();
        ComboBoxEmp();
        ComboBoxPersonal();
        ComboBoxEjecucion1();
        ComboBoxEjecucion2();
        ComboBoxEstatus();
        ComboBoxOpciones();
        int a = 0;
        TextBox1.Text = "";
        TextBox2.Text = "";

        //limpiar DropDownList
        //for (int i = 2; i <= 14; i++)
        //{
        //    ((DropDownList)this.PaneldeEdicion.FindControl("DropDownList" + i.ToString())).SelectedValue = "0";
        //}

        //limpiar TextBox
        //for (int i = 2; i <= 14; i++)
        //{
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = "";
        //}

        //limpiar Label
        //for (int i = 2; i <= 14; i++)
        //{
        //    ((Label)this.PaneldeEdicion.FindControl("Label" + i.ToString())).Text = "";
        //}

        //Combo
        DropDownList1.SelectedValue = "0";
        DropDownList2.SelectedValue = "0";
        DropDownList3.SelectedValue = "0";
        DropDownList4.SelectedValue = "0";
        DropDownList5.SelectedValue = "0";
        DropDownList6.SelectedValue = "0";
        DropDownList7.SelectedValue = "0";
        DropDownList8.SelectedValue = "0";
        DropDownList9.SelectedValue = "0";
        DropDownList10.SelectedValue = "0";
        DropDownList11.SelectedValue = "0";
        DropDownList12.SelectedValue = "0";
        DropDownList13.SelectedValue = "0";
        DropDownList14.SelectedValue = "0";
        return a;
    }

    protected int ValidadordeCaracteresEspeciales()
    {
        int a = 0;
        //Quita Valores Especiales del texbox
        //for (int i = 1; i <= 2; i++)
        //{
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("'", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¡", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("¿", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("!", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("?", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(",", "");
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(".", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("´", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("`", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(";", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("+", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("-", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("<", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace(">", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("=", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("&", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("%", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("$", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("#", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("/", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Trim();
        //    //((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.ToUpper();
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("SELECT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DELETE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("UPDATE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("INSERT ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("ALL ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("TABLE ", "");
        //    ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text.Replace("DROP ", "");

        //    if (((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text == "")
        //    {
        //        ((TextBox)this.PaneldeEdicion.FindControl("TextBox" + i.ToString())).Text = "Sin Datos";
        //    }
        //}
        ////llena con un cero
        //if (TextBox3.Text == "")
        //{
        //    TextBox3.Text = "0";
        //}
        return a;
    }

    protected int limpiarFormularioBusqueda()
    {
        //Limpia el Formulario de Busqueda
        txtBusqueda.Text = null;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        return 1;
    }


}