﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Sign_UP.aspx.cs" Inherits="Sign_UP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .bd-placeholder-img
        {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        @media (min-width: 768px)
        {
            .bd-placeholder-img-lg
            {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server" class="form-signin">
    <div>
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <div  style="text-align:center">
            <asp:UpdatePanel ID="PanelValidar" runat="server">
                <ContentTemplate>
                   <img class="mb-4" src="imagen/Logo.png" alt="" width="180px">
                    <h1 class="h3 mb-3 font-weight-normal">
                        Por favor ingrese sus datos</h1>
                    <label for="inputEmail" class="sr-only">
                        E-mail</label>
                    <asp:TextBox runat="server" ID="inputEmail" class="form-control" placeholder="E-mail"
                        required autofocus></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="TinputEmail_FilteredTextBoxExtender" runat="server"
                        Enabled="True" TargetControlID="inputEmail" ValidChars="abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ.0123456789@_-">
                    </asp:FilteredTextBoxExtender>
                    <label for="inputPassword" class="sr-only">
                        Contraseña</label>
                    <asp:TextBox runat="server" ID="inputPassword" class="form-control" TextMode="Password"
                        placeholder="Contraseña" required></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" Text="Ingrese" class="btn btn-lg btn-primary btn-block"
                        OnClick="Button1_Click" />
                    <asp:Label runat="server" ID="Lbtexto" Text=""></asp:Label>
                    <br />
                    <ul class="list-unstyled text-small">
                        <li><a class="text-muted" href="default.aspx">Volver a la Pagina principal</a></li>
                    </ul>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    </form>
</body>
</html>
