﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class Sign_UP : System.Web.UI.Page
{
    public ClassConexion conn = new ClassConexion();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Context.Session.Timeout = 1024;
            }
        }
        catch (Exception ex)
        {
            Lbtexto.Text = ex.Message;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        dt = prc_validate_tbUsuario(inputEmail.Text.ToString(), inputPassword.Text.ToString());
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow ds in dt.Rows)
            {
                Context.Session["User"] = ds["CorreoUnoUs"].ToString();
                Context.Session["NombreUsuario"] = ds["NombreUs"].ToString();
                Context.Session["Nombre$_$Usuario"] = inputEmail.Text.ToString();
                Context.Session["Nombre+_+Usuario"] = inputPassword.Text.ToString();
                Context.Session["Tipo$_$Usuario"] = ds["TipoUsuario"].ToString();

                Context.Session["reportes"] = "1";
                Context.Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Context.Session.Clear();
            Context.Session["User"] = "";
            Context.Response.Redirect("Sign_Up.aspx");
        }
    }

    //
    public DataTable prc_validate_tbUsuario(string correo, string password)
    {
        DataTable dt = new DataTable();
        using (SqlConnection cnn = new SqlConnection(conn.conection))
        {
            cnn.Open();
            using (SqlCommand cmd = new SqlCommand("validaInicio", cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 999999;
                cmd.Parameters.AddWithValue("@CorreoUnoUs", correo);
                cmd.Parameters.AddWithValue("@PasswordUs", password);

                SqlDataAdapter SqlDa = new SqlDataAdapter(cmd);
                SqlDa.SelectCommand.CommandTimeout = 999999;
                SqlDa.Fill(dt);
                SqlDa.SelectCommand.Cancel();
                cmd.Cancel();
            }
            cnn.Close();
        }
        return dt;
    }
}