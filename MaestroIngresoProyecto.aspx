﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="MaestroIngresoProyecto.aspx.cs" Inherits="MaestroIngresoProyecto" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>Maestro de Proyectos</h2>
    <asp:UpdatePanel ID="PanelPrincipal" runat="server">
        <ContentTemplate>
            <asp:UpdatePanel ID="PaneldeBusqueda" runat="server">
                <ContentTemplate>
                    <table cellpadding="0" cellspacing="0" border="0" width="90%" align="center">
                        <tr>
                            <td colspan="3">
                                <asp:Button ID="Button4" runat="server" CssClass="btn btn-warning btn-sm  " OnClick="Button4_Click"
                                    Text="Nuevo Registro" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Buscar por Descripción
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtBusqueda" runat="server" CssClass=" form-control w-100 "></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Buscar" CssClass="btn btn-warning btn-sm  float-end" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center">
                                &nbsp;
                            </td>
                            <td align="right">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="divGrid" style="overflow: auto; height: auto; width: auto;">
                                    <%--asd--%>
<%--                                    <asp:GridView ID="DataGrid1" runat="server" CssClass="table table-striped table-bordered" OnItemCommand="Grid_Ejemplo" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" onselectedindexchanging="SubmitOnPageIndexChanging">
                                    
                                    </asp:GridView>--%>

                                    <asp:DataGrid ID="DataGrid1" runat="server" CssClass="table table-striped table-bordered" OnItemCommand="Grid_Ejemplo">
                                        <Columns>
                                            <asp:ButtonColumn CommandName="Select" runat="server" Text="Seleccionar"></asp:ButtonColumn>
                                        </Columns>
<%--                                        <Columns>
                                            <asp:ButtonColumn CommandName="Update" Text="Desactivar"></asp:ButtonColumn>
                                        </Columns>--%>
                                    </asp:DataGrid>
                                </div>
                                &nbsp;                                
                                <br />

                                <asp:Button ID="btnPdf" runat="server" CssClass="btn btn-warning btn-sm" Text="Descargar información en Excel"   OnClientClick="window.open('ListaDescarga.aspx', '_blank', 'menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes,width=980px,height=450'); return false;" />
                                
                                <%--<asp:Button ID="btnCorreo" runat="server" CssClass="btn btn-warning btn-sm" Text="Enviar información al correo" onclick="btnCorreo_Click" />--%>
                                <%--Botones--%>
                               
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="Error1" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <asp:UpdatePanel ID="PaneldeEdicion" runat="server">
                <ContentTemplate>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
                        <%--                        <tr>
                            <td>
                                Mes ingreso proyecto
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <%--elias fijate en los label no llevan clase de CCS--%>
                                <asp:Label ID="label1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Descripcion del proyecto
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                            <tr>
                                <td>
                                    Usuario del sistema
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cliente
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="dropdown" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Empresa
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownList3" runat="server" CssClass="dropdown" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Personal a cargo
                                </td>
                            </tr>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList4" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                24 horas
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList5" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Adjudicacion proyecto
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList6" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ingenieria 5 dias
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList7" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Primera ejecucion 
                            </td>
                        </tr>
                        <br />
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList8" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ingenieria inversa 30 dias
                                <br />
                                <asp:DropDownList ID="DropDownList9" runat="server" CssClass="dropdown" >
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
<%--                            <asp:DropDownList ID="DropDownList9" runat="server" CssClass="dropdown" >
                            </asp:DropDownList>--%>
                        </tr>
                        <tr>
                            <td>
                                Observacion del proyecto
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" CssClass=" form-control" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Segunda ejecucion
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList10" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ingreso del proyecto
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList11" runat="server" CssClass="dropdown">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cierre del proyecto
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList12" runat="server" CssClass="dropdown">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Entregables del proyecto
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList13" runat="server" CssClass="dropdown">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estado del registro
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList14" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <%--asd--%>
                        <tr>
                            <td>
                                <br />
                                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Grabar" CssClass="btn btn-warning btn-sm  " />
                                &nbsp;<asp:Button ID="Button3" runat="server" OnClick="Button3_Click" CssClass="btn btn-warning btn-sm  "
                                    Text="Volver" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Error2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <%--fin tabla--%>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--FIN--%>
</asp:Content>
