﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListaDescarga.aspx.cs" Inherits="ListaDescarga" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet" />
      <style type="text/css">
        .bd-placeholder-img
        {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }
        @media (min-width: 768px)
        {
            .bd-placeholder-img-lg
            {
                font-size: 3.5rem;
            }
        }
        
        
    </style>
    <title>Detalle</title>
</head>
<body style="font-family: Calibri Light; font-size: small">
    <form id="form1" runat="server">
   <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click"  CssClass="btn btn-warning btn-sm  float-right" Text="Exportar documento" />
                &nbsp;
                <asp:DropDownList runat="server" ID="Di" CssClass="dropdown">
                <asp:ListItem Value="0">Seleccione</asp:ListItem>
                <asp:ListItem Value="1">Excel</asp:ListItem>
                <asp:ListItem Value="2">PDF</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:GridView ID="GridView1" runat="server"  OnPageIndexChanging="OnPageIndexChanging">
                </asp:GridView>
                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="GridView1" />
                <asp:PostBackTrigger ControlID="btnExportExcel" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
