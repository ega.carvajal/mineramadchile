﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true"
    CodeFile="MaestroOrdenCompra.aspx.cs" Inherits="MaestroOrdenCompra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--INICIO--%>
    <h2>Maestro Orden de compra</h2>
    <asp:UpdatePanel ID="PanelPrincipal" runat="server">
        <ContentTemplate>
            <asp:UpdatePanel ID="PaneldeBusqueda" runat="server">
                <ContentTemplate>
                    
                    <table cellpadding="0" cellspacing="0" border="0" width="90%" align="center">
                        <tr>
                            <td colspan="3">
                                <asp:Button ID="Button4" runat="server" CssClass="btn btn-warning btn-sm  " OnClick="Button4_Click"
                                    Text="Nuevo Registro" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Buscar por Descripción
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtBusqueda" runat="server" CssClass=" form-control w-100 "></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Buscar" CssClass="btn btn-warning btn-sm  float-end" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center">
                                &nbsp;
                            </td>
                            <td align="right">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="divGrid" style="overflow: auto; height: auto; width: auto;">
                                    <asp:DataGrid ID="DataGrid1" runat="server" CssClass="table table-striped table-bordered" OnItemCommand="Grid_Ejemplo">
                                        <Columns>
                                            <asp:ButtonColumn CommandName="Select" Text="Seleccionar"></asp:ButtonColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="Error1" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <asp:UpdatePanel ID="PaneldeEdicion" runat="server">
                <ContentTemplate>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="900px">
                        <tr>
                            <td>
                                Orden de compra
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SAP
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Empresa
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estado de compra
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Descripcion del producto
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox3" runat="server" CssClass=" form-control" MaxLength="1000" TextMode="MultiLine"> </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cantidad de unidades
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox4" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Valor Unitario
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox5" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Valor Total
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox6" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Guia de Despacho
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox7" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Factura
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox8" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Planos
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox9" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fecha 1
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox10" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fecha 2
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox11" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Entrega
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownList3" runat="server" CssClass="dropdown" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Mes
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox12" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Control de calidad
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox13" runat="server" CssClass=" form-control"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Grabar" CssClass="btn btn-warning btn-sm  " />
                                &nbsp;<asp:Button ID="Button3" runat="server" OnClick="Button3_Click" CssClass="btn btn-warning btn-sm  "
                                    Text="Volver" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Error2" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--FIN--%>
</asp:Content>
